# coding: utf-8
class Quest:
	def __init__(self):
		self.choice=0
		self.broken_feet=False
		self.ending=""
		self.password=0

	def show_text(self, text):
		print text

	def players_input(self):
		print u"\nВыберите действие:\n"
		x=0
		x+=int(raw_input())
		print
		return x

	def pause(self):
		self.show_text(u"\n...Нажите Enter чтобы продолжить...\n")
		raw_input()
		print


####################################################################################################################
	def scene1(self):
		self.show_text(u"\n.....Вы просыпаетесь на операционном столе..... \n"+
			u"Осмотревшись в комнате вы видете много хирургических принадлежностей \n"+
			u"Это однозначно больничная комната. \n"+
			u"В глазах мутно после наркоза.. \n")
		self.pause()
		self.show_text(u"'Слишком много крови',- думаете вы \n"+
			u"Посмотрев налево вы видите расчлененный труп. \n"+
			u"Он лежит на таком же операционном столе. \n")
		self.pause()
		self.show_text(u"За дверью вы слышите разговор двух мужчин..... \n"+"\n"+
			u"__Голос за дверью__:ПОСЛЕ ПЕРЕКУРА ЗАЙМЕМСЯ ВТОРЫМ \n"+
			u".....................................\n"+
			u"Кажется они уходят....... \n")
		self.show_text(u"Вы понимаете что это ваш шанс выбраться отсюда \n")
		self.show_text(u"Выйдя в коридор видите открытое окно в одном конце коридора\n"+
			u"в другом конце вы видите лестницу на нижний этаж, \n"+
			u"а перед вами табличка с надписью:-'2016 год - год спасения' \n")

		self.show_text(u"Введите цифру соответствующую вашему действию \n"+
			u"1 - Двигаться к лестнице \n"+
			u"2 - Двигаться к окну \n")
		
		while True:
			self.choice+=self.players_input()
			if self.choice==1:
				self.choice=0
				self.first_floor()
				break
			
			elif self.choice==2:
					self.choice=0
					self.to_window()
					break
			elif self.choice!=2:
					self.show_text(u"!!!Некорректное действие!!!")
		if self.ending=="good":
			self.show_text(u"Вам останется забыть все это как страшный сон.......")
		elif self.ending=="bad":
			self.show_text(u"Больше вас никто и никогда не видел.......")
		print
		self.show_text(u"_____GAME OVER______")		
					

	def to_window(self):
		self.show_text(u"Вы подходите к открытому окну и глядя в него понимаете, \n"+
			u"что вы на втором этаже, но внизу вы видите забор через который вы сможете \n"+
			u"перепрыгнуть чтобы сбежать.\n"+
			u"1 - Выпрыгнуть в окно \n"+
			u"2 - Обернуться и поискать другие пути \n")
		while True:
			self.choice=self.players_input()
			if self.choice==1:
				self.broken_feet=True
				self.choice=0
				self.show_text(u"Выпрыгнув из окна вы ломаете лодыжку...\n"+
					u"ужасная боль сковывает вас, и вдруг.... \n")
				self.pause()
				self.outside()
				break
			
			elif self.choice==2:
				self.choice=0
				self.show_text(u"Обернувшись вы слышите шаги направляющиеся к вам....\n"+
					u"Вы прячетесь в комнате, дверь в которую была открыта \n"+
					u"Шаги слышатся все ближе и ближе........ \n")
				self.pause()
				self.show_text(u"Вы видите что в комнате есть выход на пожарную лестницу...\n"+
					u"По ней вы спускаетесь вниз... \n")
				self.pause()					
				self.outside()
				break

			elif self.choice!=2:
					self.show_text(u"!!!Некорректное действие!!!")



	def outside(self):

		self.show_text(u"Вас замечает, услышавший шум, охранник..\n"+
					u"Он бежит к вам с приказом остановиться...\n")
		
		if self.broken_feet==True:
			self.ending="bad"
			self.show_text(u"Вас сковывет боль, вы не можете ничего сделать..\n"+
				u"Получив удар электрошокером вы теряете сознание...\n")
			

		else:
			self.ending="good"
			self.show_text(u"Вам ничего не остается кроме как перепрыгнуть через забор и бежать..\n"+
				u"...................................................................\n"
				u"Пробежав 3 квартала вы отрываетесь от преследователя\n"+
				u"Отдышавшись вы понимаете, что спаслись....\n")

	def first_floor(self):
		self.show_text(u"Вы спускаетесь на первый этаж, и видите выходную дверь, НО... \n"+"\n"+
			u"Подойдя ближе вы видите, что на вахте спит охранник, а дверь \n"+
			u"закрыта на кодовый замок. Открыть его - ваш единственный путь к спасению\n"+
			u"Подойдя к двери вы пытаетесь ввести пароль \n")
		for i in range(3):
			self.password=int(raw_input())
			if self.password==2016:
				self.show_text(u"На экране появляется заветное:'Доступ открыт'' \n")
				break
			else:
				self.show_text(u"Пароль неверный \n")
		if self.password==2016:
			self.ending="good"
			self.show_text(u"Вы выходите в открытую парадную дверь оставшись незамеченным \n"+"\n"+
			u"Выйдя из больницы вы спешите уйти подальше от этого проклятого места \n")
		else:
			self.ending="bad"
			self.show_text(u"Вы чувствуете удар электрошокера в шею и теряете сознание...... \n")
game=Quest()
game.scene1()



			





